#include "type_traits.h"
#include <type_traits>

// In each test, replace XXX with syntax that passes the test.

TEST_CASE("is_void")
{
    REQUIRE(std::is_void<void>::value);
    std::is_void<void> val;
    REQUIRE(static_cast<bool>(val));
}

TEST_CASE("is_integral")
{
    REQUIRE(std::is_integral<int>::value);
    REQUIRE(false == std::is_integral<int &>::value); // GOT WRONG this is false, not true; references are not integral
    REQUIRE(false == std::is_integral<int *>::value);
    REQUIRE(true == std::is_integral<decltype('!')>::value); // char is (-128)–128
    REQUIRE(true == std::is_integral<decltype(false)>::value); // bool is 0, 1
    REQUIRE(true == std::is_integral<decltype(666ULL)>::value); // unsigned long long
    REQUIRE(false == std::is_integral<decltype(0.)>::value); // nope, 0. is float
    REQUIRE(false == std::is_integral<decltype(0.f)>::value); // still float
    REQUIRE(false == std::is_integral<decltype("Hello, world!")>::value); // array of char not integral
    REQUIRE(true == std::is_integral<decltype(L'!')>::value); // wchar_t still integral
    REQUIRE(false == std::is_integral<decltype(L"Hello, world!")>::value); // wchar_t[] not integral
    REQUIRE(false == std::is_integral<decltype(R"(Hello, "world"!)")>::value); // still array, nope
    REQUIRE(true == std::is_integral<const int>::value); // const, but still integral
}

TEST_CASE("is_floating_point")
{
    REQUIRE(false == std::is_floating_point<int>::value); // int not a float
    REQUIRE(true == std::is_floating_point<float>::value);
    REQUIRE(true == std::is_floating_point<double>::value);
    REQUIRE(true == std::is_floating_point<long double>::value);
    REQUIRE(true == std::is_floating_point<decltype(0 + 1.)>::value);
    REQUIRE(true == std::is_floating_point<decltype(0. + 1)>::value);
    REQUIRE(false == std::is_floating_point<decltype(0 + 1)>::value); // pointer not a float
}

TEST_CASE("is_array")
{
    REQUIRE(false == std::is_array<int>::value); // int is not an array
    typedef std::array<int, 10> std_array;
    REQUIRE(false == std::is_array<std_array>::value); // GOT WRONG false, not true because std::array is a
                                                       // class that encapsulates fixed sized arrays
    REQUIRE(false == std::is_array<int *>::value); // pointer is not array
    REQUIRE(false == std::is_array<int &>::value); // reference is not array
    REQUIRE(std::is_array<bool[]>::value);
    REQUIRE(true == std::is_array<int *[10]>::value);
}

TEST_CASE("is_enum")
{
    enum Weekend{Saturday, Sunday};
    REQUIRE(std::is_enum<Weekend>::value);
}

TEST_CASE("is_union")
{
    typedef union {int a;} U;
    REQUIRE(std::is_union<U>::value);
    struct S {};
    REQUIRE(false == std::is_union<S>::value);
}

TEST_CASE("is_class")
{
    REQUIRE(false == std::is_class<int>::value);
    struct S {};
    REQUIRE(true == std::is_class<S>::value);
    class C {};
    REQUIRE(true == std::is_class<C>::value);
    union U {};
    REQUIRE(false == std::is_class<U>::value); // GOT WRONG false, not true because is_class
                                               // checks for NON-UNION class type
    typedef int A[10];
    REQUIRE(false == std::is_class<A>::value);
}

TEST_CASE("is_function")
{
    struct S {};
    S s1;
    REQUIRE(false == std::is_function<decltype(s1)>::value);
    S s2{};
    REQUIRE(false == std::is_function<decltype(s2)>::value);
    REQUIRE(true == std::is_function<void()>::value);
    typedef void p();
    REQUIRE(true == std::is_function<p>::value);
    REQUIRE(false == std::is_function<p *>::value);
    REQUIRE(false == std::is_function<std::function<void()>>::value); // GOT WRONG std::function is a
                                                                      // class that encapsulates function
}

TEST_CASE("is_pointer")
{
    REQUIRE(false == std::is_pointer<void>::value);
    REQUIRE(true == std::is_pointer<void *>::value);
    int i;
    REQUIRE(true == std::is_pointer<decltype(&i)>::value);
    REQUIRE(false == std::is_pointer<decltype('&')>::value);
    REQUIRE(false == std::is_pointer<decltype("Hello, world!")>::value); // GOT WRONG not true, const char[] is array not pointer
    REQUIRE(true == std::is_pointer<decltype("Hello, world!" + 1)>::value);
}

TEST_CASE("is_lvalue_reference")
{
    REQUIRE(false == std::is_lvalue_reference<int>::value);
    REQUIRE(true == std::is_lvalue_reference<int &>::value);
    REQUIRE(false == std::is_lvalue_reference<int &&>::value);
    int i;
    REQUIRE(false == std::is_lvalue_reference<decltype(&i)>::value);
}

TEST_CASE("is_rvalue_reference")
{
    REQUIRE(false == std::is_rvalue_reference<int>::value);
    REQUIRE(false == std::is_rvalue_reference<int &>::value);
    REQUIRE(true == std::is_rvalue_reference<int &&>::value);
    int i;
    REQUIRE(false == std::is_rvalue_reference<decltype(&i)>::value);
    REQUIRE(true == std::is_rvalue_reference<decltype(std::move(i))>::value);
}

TEST_CASE("is_member_object_pointer")
{
    struct S { int i; };
    S s;
    REQUIRE(false == std::is_member_object_pointer<decltype(&s)>::value); // GOT WRONG not true, must be because pointer to instance isn't member object
    REQUIRE(true == std::is_member_object_pointer<decltype(&S::i)>::value);
    REQUIRE(true == std::is_member_object_pointer<int(S::*)>::value); // GOT WRONG int part is confusing
    union U { int i; char c; };
    U u;
    REQUIRE(false == std::is_member_object_pointer<decltype(&u)>::value);
    REQUIRE(true == std::is_member_object_pointer<decltype(&U::i)>::value);
    REQUIRE(true == std::is_member_object_pointer<int(U::*)>::value);
}

TEST_CASE("is_member_function_pointer")
{
    struct S { char c; void i() {} };
    S s;
    REQUIRE(false == std::is_member_function_pointer<decltype(&s)>::value);
    REQUIRE(true == std::is_member_function_pointer<decltype(&S::i)>::value);
    REQUIRE(false == std::is_member_function_pointer<decltype(&S::c)>::value);
    REQUIRE(true == std::is_member_function_pointer<void(S::*)()>::value); // GOT WRONG
}

TEST_CASE("is_arithmetic")
{
    REQUIRE(!std::is_arithmetic<void>::value); // void is not arithmetic type
    REQUIRE(!std::is_arithmetic<std::nullptr_t>::value); // nullptr_t is not arithmetic type
    REQUIRE(std::is_integral<bool>::value == std::is_arithmetic<bool>::value); // integrals are arithmetic
    REQUIRE(std::is_floating_point<float>::value == std::is_arithmetic<float>::value); // floats are arithmetic
    REQUIRE(true == std::is_arithmetic<char>::value); // chars are arithmetic
    REQUIRE(true == std::is_arithmetic<char16_t>::value); // same
    REQUIRE(true == std::is_arithmetic<char32_t>::value); // same
    REQUIRE(true == std::is_arithmetic<wchar_t>::value); // same
    REQUIRE(true == std::is_arithmetic<unsigned char>::value); // same
    REQUIRE(std::is_arithmetic<std::vector<int>::value_type>::value); // value type of vector is arithmetic
}

TEST_CASE("is_fundamental")
{
    // fundamental types are arithmetic, void, and nullptr_t
    REQUIRE(true == std::is_fundamental<void>::value);
    REQUIRE(true == std::is_fundamental<std::nullptr_t>::value);
    REQUIRE(std::is_arithmetic<bool>::value == std::is_fundamental<bool>::value);
    REQUIRE(true == std::is_fundamental<char>::value);
    REQUIRE(true == std::is_fundamental<unsigned char>::value);
    REQUIRE(true == std::is_fundamental<int>::value);
    REQUIRE(true == std::is_fundamental<double>::value);
    REQUIRE(false == std::is_fundamental<std::string>::value); // string/char array not fundamental
    REQUIRE(false == std::is_fundamental<decltype("Hello, world!")>::value); // same
    REQUIRE(false == std::is_fundamental<decltype("Hello, world!" + 1)>::value); // pointer not fundamental
    REQUIRE(false == std::is_fundamental<std::vector<int *>>::value); //
    REQUIRE(false == std::is_fundamental<std::vector<int> *>::value);
    REQUIRE(true == std::is_fundamental<std::vector<int>::value_type>::value);
}

TEST_CASE("is_const")
{
    // WEAK AREA STUDY plz
    REQUIRE(false == std::is_const<int>::value);
    REQUIRE(true == std::is_const<const int>::value); // const int is const
    REQUIRE(false == std::is_const<int const *>::value); // const pointer to int is const WRONG?
    REQUIRE(false == std::is_const<const int *>::value); // non-const pointer to constant int
    REQUIRE(true == std::is_const<int *const>::value); // pointer is const qualified (values pointed by int pointer can't be changed)
    REQUIRE(true == std::is_const<int const *const>::value); // const pointer to int is const, can't change pointer or value pointed to
    REQUIRE(false == std::is_const<int const &>::value); // const reference, which is not const
}

TEST_CASE("is_volatile")
{
    // volatile essentially means to compiler "can't be optimized, don't mess with this"
    REQUIRE(false == std::is_volatile<int>::value); // no volatile qualifier
    REQUIRE(true == std::is_volatile<volatile int>::value); // yep
    REQUIRE(false == std::is_volatile<int volatile *>::value); // volatile pointer to int is volatile
    REQUIRE(false == std::is_volatile<volatile int *>::value); // non-volatile pointer to volatile int
    REQUIRE(true == std::is_volatile<int *volatile>::value); // non-volatile (values pointed by int pointer can't be optimized?)
    REQUIRE(true == std::is_volatile<int volatile *volatile>::value); // volatile pointer to int is volatile
    REQUIRE(false == std::is_volatile<int volatile &>::value); // volatile reference which is not volatile
}

TEST_CASE("is_trivial")
{
    // trival translates to "trivially copyable," meaning the object
    // is stored 100% in contiguous memory
    REQUIRE(true == std::is_trivial<int>::value);
    class Empty {};
    REQUIRE(true == std::is_trivial<Empty>::value);
    REQUIRE(true == std::is_trivial<Empty[10]>::value);
    class CustomConstructor
    {
    public:
        CustomConstructor(int i) : i_(i) {}
    private:
        int &i_;
    };
    REQUIRE(false == std::is_trivial<CustomConstructor>::value);
}
