#include "c_strings.h"

TEST_CASE("type of character literals")
{
    REQUIRE((std::is_same_v<decltype('a'), const char>)); // vanilla string literal
    REQUIRE((std::is_same_v<decltype(L'a'), const wchar_t>)); // wide string literal
    REQUIRE((std::is_same_v<decltype(u'a'), const char16_t>)); // UTF-16 string literal
    REQUIRE((std::is_same_v<decltype(U'a'), const char32_t>)); // UTF-32 string literal
    REQUIRE((std::is_same_v<decltype(u8'a'), const char>)); // UTF-8 string literal (const char8_t C++20)
}

TEST_CASE("special character literals")
{
    // see <https://en.wikipedia.org/wiki/File:USASCII_code_chart.png>
    REQUIRE(16 == static_cast<int>('\020'));
    REQUIRE(32 == static_cast<int>('\x20'));
    REQUIRE(7 == static_cast<int>('\a'));
    REQUIRE(8 == static_cast<int>('\b'));
    REQUIRE(12 == static_cast<int>('\f'));
    REQUIRE(10 == static_cast<int>('\n'));
    REQUIRE(13 == static_cast<int>('\r'));
    REQUIRE(9 == static_cast<int>('\t'));
    REQUIRE(11 == static_cast<int>('\v'));
    REQUIRE(39 == static_cast<int>('\''));
    REQUIRE(34 == static_cast<int>('\"'));
    REQUIRE(63 == static_cast<int>('\?'));
    REQUIRE(92 == static_cast<int>('\\'));
    REQUIRE(4660 == static_cast<int>(L'\x1234'));
    REQUIRE(4660 == static_cast<int>(u'\u1234'));
    REQUIRE(7680 == static_cast<int>(U'\U00001E00'));
    REQUIRE(65 == static_cast<int>(u8'A'));
}

TEST_CASE("type of string literals")
{
    auto s1{"Hello, world!"};
    auto s2{L"Hello, world!"};
    auto s3{u"Hello, world!"};
    auto s4{U"Hello, world!"};
    auto s5{u8"Hello, world!"};
    REQUIRE((std::is_same_v<decltype(s1), const char[]>));
    REQUIRE((std::is_same_v<decltype(s2), const wchar_t[]>));
    REQUIRE((std::is_same_v<decltype(s3), const char16_t[]>));
    REQUIRE((std::is_same_v<decltype(s4), const char32_t[]>));
    REQUIRE((std::is_same_v<decltype(s5), const char[]>));
}

TEST_CASE("type of raw string literals")
{
    auto r1{R"one(Hello, world!)one"};
    auto r2{LR"two(Hello, world!)two"};
    auto r3{uR"three(Hello, world!)three"};
    auto r4{UR"four(Hello, world!)four"};
    auto r5{u8R"five(Hello, world!)five"};
    REQUIRE((std::is_same_v<decltype(r1), const char[]>));
    REQUIRE((std::is_same_v<decltype(r2), const wchar_t[]>));
    REQUIRE((std::is_same_v<decltype(r3), const char16_t[]>));
    REQUIRE((std::is_same_v<decltype(r4), const char32_t[]>));
    REQUIRE((std::is_same_v<decltype(r5), const char[]>));
}

TEST_CASE("raw string literals")
{
    auto s{R"lit(
Hello, world!
)lit"};
    REQUIRE('H' == s[0]);
}

TEST_CASE("begin end on character arrays")
{
    char s[]{"Hello, world!"};

    REQUIRE(s == std::begin(s));
    REQUIRE(s + 13 == std::end(s));
}
